# GBASH

## 1 Description

<😊 PLACEHOLDER_DESCRIPTION 😊>

## 1 Variables

| Property         | Value  |
| ---------------- | ------ |
| `<project-name>` | gbash  |
| `<output-path>`  | public |

## 2 INITIAL SETUP

### 2.1 Visual Studio Code

#### 2.1.1 Undo last commit

> :information_source: Note:<br>For a clean first commit undo last one and commit after you did the following steps of chapter 2.

#### 2.1.2 Edit .gitignore

```gitignore
# compiled output
...
/<output-path>
/.firebase
firebase-debug.log

# sensitive data
.env

...
```

### 2.2 Create Environments

#### 2.2.1 .env/credentials.ts

```typescript
export const credentials = {
  sentryDNS = '<SENTRY_DNS>'
};
```

#### 2.2.2 src/environment/environment.ts

```typescript
import { credentials } from '../../.env/credentials';
import 'zone.js/dist/zone-error';

export const environment = {
  production: false,
  hmr: false,
  sentryDNS: credentials.sentryDNS,
  ngxsDebuggerDisabled: false,
  firebase: {}
};
```

#### 2.2.3 .env/credentials.prod.ts

```typescript
export const credentials = {
  sentryDNS = '<SENTRY_DNS_PROD>'
};
```

#### 2.2.4 src/environment/environment.prod.ts

```typescript
import { credentials } from '../../.env/credentials.prod';

export const environment = {
  production: true,
  hmr: false,
  sentryDNS: credentials.sentryDNS,
  ngxsDebuggerDisabled: true,
  firebase: {}
};
```

#### 2.2.5 .env/credentials.staging.ts

```typescript
export const credentials = {
  sentryDN = '<SENTRY_DNS_STAGING>'
};
```

#### 2.2.6 src/environment/environment.staging.ts

```typescript
import { CREDENTIALS } from '../../.env/credentials.staging';
import 'zone.js/dist/zone-error';

export const environment = {
  production: true,
  hmr: false,
  sentryDNS: credentials.sentryDNS,
  ngxsDebuggerDisabled: true,
  firebase: {}
};
```

#### 2.2.7 .env/credentials.hmr.ts

```typescript
export const credentials = {
  sentryDN = '<SENTRY_DNS>'
};
```

#### 2.2.8 src/environment/environment.hmr.ts

```typescript
import { CREDENTIALS } from '../../.env/credentials.hmr';
import 'zone.js/dist/zone-error';

export const environment = {
  production: false,
  hmr: true,
  sentryDNS: credentials.sentryDNS,
  ngxsDebuggerDisabled: false,
  firebase: {}
};
```

### 2.3 OUTPUT PATHS

> :information_source: Note:<br> Since Angular uses `dist` and firebase `public`. Unify the output.

#### 1.3.1 angular.json

```json
...
"architect": {
  "build": {
    "builder": "@angular-devkit/build-angular:browser",
    "options": {
      "outputPath": "<output-path>/<project-name>",
      ...
```

#### 2.3.2 tsconfig.json

```json
...
  "compilerOptions": {
    ...
    "outDir": "./<output-path>/out-tsc",
```

### 2.4 SETUP HMR

#### 2.4.1 Edit tsconfig.app.json

```json
{
  ...
  "compilerOptions": {
    "outDir": "../out-tsc/app",
    "types": ["node"]
```

#### 2.4.2 Edit main.ts

```typescript
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .catch(err => console.error(err));
```

#### 2.4.3 Install @angularclass/hmr

```bash
npm i -D @angularclass/hmr
```

#### 2.4.4 Create `src/hmr.ts`

```typescript
import { ApplicationRef, NgModuleRef } from '@angular/core';
import { createNewHosts } from '@angularclass/hmr';

export const hmrBootstrap = (
  module: any,
  bootstrap: () => Promise<NgModuleRef<any>>
) => {
  let ngModule: NgModuleRef<any>;
  module.hot.accept();
  bootstrap().then(mod => (ngModule = mod));
  module.hot.dispose(() => {
    const appRef: ApplicationRef = ngModule.injector.get(ApplicationRef);
    const elements = appRef.components.map(c => c.location.nativeElement);
    const makeVisible = createNewHosts(elements);
    ngModule.destroy();
    makeVisible();
  });
};
```

#### 2.4.5 Edit `package.json`

```json
...
"scripts": {
  "ng": "ng",
  "start": "ng serve",
  "build": "ng build",
  "test": "ng test",
  "lint": "ng lint",
  "e2e": "ng e2e",
  "hmr": "ng serve --configuration hmr",
  ...
},
...
```

## GIT

## CONFIGURATIONS
