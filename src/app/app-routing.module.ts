import { NgModule } from '@angular/core';
import { PreloadingModuleService } from './core/stategies/preloading-module.service';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'signin',
    loadChildren: './signin/signin.module#SigninModule',
    data: { preload: true, delay: false }
  },
  {
    path: '',
    loadChildren: './shell/shell.module#ShellModule',
    data: { preload: true, delay: false }
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadingModuleService
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
