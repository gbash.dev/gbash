import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  DoCheck,
  OnChanges,
  SimpleChanges
  } from '@angular/core';
import { AppLoadingState } from './core/app-loading/app-loading.state';
import { AppLoadingStateModel } from './core/app-loading/app-loading.model';
import { Emittable, Emitter } from '@ngxs-labs/emitter';
import { fadeAnimation } from './core/animations/router.animations';
import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';

@Component({
  selector: 'gbash-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [fadeAnimation]
})
export class AppComponent implements AfterViewInit {
  @Emitter(AppLoadingState.hideAppLoadingScreen)
  public hideAppLoadingScreen: Emittable<AppLoadingStateModel>;

  @Select(AppLoadingState.getAppLoadingState)
  public appLoadingState$: Observable<AppLoadingStateModel>;

  constructor(private _cd: ChangeDetectorRef) {}

  ngAfterViewInit() {
    this.hideAppLoadingScreen.emit();
    this._cd.detectChanges();
  }
}
