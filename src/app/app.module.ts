import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { CoreModule } from './core/core.module';
import { environment } from '../environments/environment';
import { NgModule } from '@angular/core';
import { NgxsEmitPluginModule } from '@ngxs-labs/emitter';
import { NgxsFormPluginModule } from '@ngxs/form-plugin';
import { NgxsHmrLifeCycle, NgxsStoreSnapshot } from '@ngxs/hmr-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsModule, StateContext } from '@ngxs/store';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { ServiceWorkerModule } from '@angular/service-worker';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production
    }),
    NgxsModule.forRoot([], {
      developmentMode: !environment.production
    }),
    NgxsEmitPluginModule.forRoot(),
    NgxsRouterPluginModule.forRoot(),
    NgxsStoragePluginModule.forRoot({
      key: ['core']
    }),
    NgxsFormPluginModule.forRoot(),
    NgxsLoggerPluginModule.forRoot({
      logger: console,
      collapsed: false,
      disabled: environment.ngxsDebuggerDisabled
    }),
    NgxsReduxDevtoolsPluginModule.forRoot({
      disabled: environment.ngxsDebuggerDisabled
    }),
    CoreModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule implements NgxsHmrLifeCycle<NgxsStoreSnapshot> {
  public hmrNgxsStoreOnInit(
    ctx: StateContext<NgxsStoreSnapshot>,
    snapshot: NgxsStoreSnapshot
  ) {
    console.log('[NGXS HMR] Current state', ctx.getState());
    console.log('[NGXS HMR] Previous state', snapshot);
    ctx.patchState(snapshot);
  }
  public hmrNgxsStoreBeforeOnDestroy(
    ctx: StateContext<NgxsStoreSnapshot>
  ): NgxsStoreSnapshot {
    const snapshot: NgxsStoreSnapshot = ctx.getState();
    console.log('[NGXS HMR] Saved state before on destroy', snapshot);
    return snapshot;
  }
}
