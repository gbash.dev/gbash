export interface AppLoadingStateModel {
  loading: boolean;
  loaded: boolean;
  message: string;
  errorMessage?: any;
}
