import { AppLoadingStateModel } from './app-loading.model';
import { LayoutStateModel } from '../layout/layout.model';
import { Receiver } from '@ngxs-labs/emitter';
import { Selector, State, StateContext } from '@ngxs/store';

@State<AppLoadingStateModel>({
  name: 'appLoading',
  defaults: {
    loading: true,
    loaded: false,
    message: 'loading ...'
  }
})
export class AppLoadingState {
  @Selector()
  static getAppLoadingState(state: AppLoadingStateModel): AppLoadingStateModel {
    return state;
  }

  @Receiver()
  static hideAppLoadingScreen({
    getState,
    patchState
  }: StateContext<AppLoadingStateModel>) {
    const state = getState();

    if (state.errorMessage) {
      delete state.errorMessage;
    }

    patchState({
      ...state,
      loading: false,
      loaded: true
    });
  }

  ngxsOnInit() {
    console.warn('AppLoadingState loading ...  ');
  }
}
