import { AppLoadingState } from './app-loading/app-loading.state';
import { CommonModule } from '@angular/common';
import { CoreState } from './core.state';
import { LayoutState } from './layout/layout.state';
import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { RouteReusableService } from './stategies/route-reusable.service';
import { RouteReuseStrategy } from '@angular/router';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NgxsModule.forFeature([CoreState, AppLoadingState, LayoutState])
  ],
  providers: [
    {
      provide: RouteReuseStrategy,
      useClass: RouteReusableService
    }
  ]
})
export class CoreModule {}
