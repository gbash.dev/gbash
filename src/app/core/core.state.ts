import { AppLoadingState } from './app-loading/app-loading.state';
import { LayoutState } from './layout/layout.state';
import { State } from '@ngxs/store';

@State({
  name: 'core',
  children: [AppLoadingState, LayoutState]
})
export class CoreState {}
