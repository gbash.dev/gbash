import { Loading } from './loading.model';

export interface LayoutStateModel {
  appLoadingState: Loading;
}
