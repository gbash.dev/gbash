export interface Loading {
  loading: boolean;
  loaded: boolean;
  message: string;
  error?: any;
}
