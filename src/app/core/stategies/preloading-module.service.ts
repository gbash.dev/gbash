import { Injectable } from '@angular/core';
import { mergeMap } from 'rxjs/operators';
import { Observable, of, timer } from 'rxjs';
import { PreloadingStrategy, Route } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PreloadingModuleService implements PreloadingStrategy {
  preload(route: Route, load: () => Observable<any>): Observable<any> {
    if (route.data && route.data['preload']) {
      console.log(
        'Preload Path: ' + route.path + ', delay:' + route.data['delay']
      );
      if (route.data['delay']) {
        return timer(5000).pipe(mergeMap(() => load()));
      }
      return load();
    } else {
      return of(null);
    }
  }
}
