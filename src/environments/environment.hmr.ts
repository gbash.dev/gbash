import { credentials } from '../../.env/credentials.prod';
import 'zone.js/dist/zone-error';

export const environment = {
  production: false,
  hmr: true,
  sentryDNS: credentials.sentryDNS,
  ngxsDebuggerDisabled: false,
  firebase: {}
};
