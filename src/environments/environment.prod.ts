import { credentials } from '../../.env/credentials.prod';
import 'zone.js/dist/zone-error';

export const environment = {
  production: true,
  hmr: false,
  sentryDNS: credentials.sentryDNS,
  ngxsDebuggerDisabled: false,
  firebase: {}
};
