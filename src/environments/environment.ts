import { credentials } from './../../.env/credentials';
import 'zone.js/dist/zone-error';

export const environment = {
  production: false,
  hmr: true,
  sentryDNS: '',
  ngxsDebuggerDisabled: false,
  firebase: {}
};
