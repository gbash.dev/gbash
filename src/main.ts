import { AppModule } from './app/app.module';
import { enableProdMode } from '@angular/core';
import { environment } from './environments/environment';
import { hmrNgxsBootstrap } from '@ngxs/hmr-plugin';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
// import { hmrBootstrap } from './hmr';
// import { MissingTranslationStrategy, TRANSLATIONS, TRANSLATIONS_FORMAT } from '@angular/core';
// import 'hammerjs';

// declare const require;
// const translations = require(`raw-loader!./locale/locale.de.xlf`);

// const bootstrap = () =>
//   platformBrowserDynamic().bootstrapModule(AppModule, {
//     missingTranslation: MissingTranslationStrategy.Error,
//     providers: [
//       { provide: TRANSLATIONS, useValue: translations },
//       { provide: TRANSLATIONS_FORMAT, useValue: 'xlf' },
//     ],
//   });

if (environment.production) {
  enableProdMode();
}
const bootstrap = () => platformBrowserDynamic().bootstrapModule(AppModule);
if (environment.hmr) {
  if (module['hot']) {
    hmrNgxsBootstrap(module, bootstrap);
  } else {
    console.error('HMR is not enabled for webpack-dev-server!');
    console.log('Are you using the --hmr flag for ng serve?');
  }
} else {
  bootstrap().catch(err => console.log(err));
}
